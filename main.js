const MAX_HEIGHT=200;
const MAX_PREVIEW_HEIGHT=800;
let isInPreviewMode=false;

const view = document.getElementById('view');
const outline = document.getElementById('showcase');

const setViewStyle = function (photo, viewstyle) {
  // Improve later
  if (photo && photo.classList && photo.classList.contains('photo')){
    photo.classList.remove('photo-overview', 'photo-minimap', 'photo-preview');
    photo.classList.add(`photo-${viewstyle}`);
  }else{
    console.log("Could not find classList of ", photo);
  }
}

const loadItem = function (inPreviewBool, item, img, md) {
  img.addEventListener('click', event => {
    console.log('Image %s was selected', img.id);
    if(!isInPreviewMode){
      isInPreviewMode=true;
      view.style.display='block';
      outline.style.overflowX='scroll';
      outline.style.overflowY='hidden';
      outline.style.flexWrap='nowrap';
      document.querySelectorAll('.gallery .outline .item').forEach(i => {
        let photo = i.firstElementChild;
        i.style.margin=null;
        photo.width*=0.5;
        photo.height*=0.5;
        i.style.width=photo.width+'px';
        i.style.height=photo.height+'px';
        i.classList.remove('photo-active');
        setViewStyle(i, 'minimap');
      });
      console.log('Switched to preview mode');
    }
    
    if (view.lastElementChild && view.lastElementChild.localName == 'img') {
      view.lastElementChild.remove();
    }

    document.querySelectorAll('.photo-active').forEach(activePhoto => {
      activePhoto.classList.remove('photo-active');
    });

    let scale = MAX_PREVIEW_HEIGHT/md.height; 
    let previewImage = new Image(md.width * scale , MAX_PREVIEW_HEIGHT);
    previewImage.id=`image-${md.id}`;
    previewImage.src = md.download_url;
    view.appendChild(previewImage);

    
    item.classList.add('photo-active');
  });
  setViewStyle(item, 'overview');
};

const loadData = function () {
  const outline = document.getElementById('showcase');
  fetch('https://picsum.photos/v2/list')
    .then(response => {
      return response.json();
    })
    .then(data => {
      data.forEach(it =>{
        let item = document.createElement('div');
        item.classList.add('item', 'photo');
        let scale = MAX_HEIGHT/it.height; 
        img = new Image(it.width * scale , MAX_HEIGHT);
        img.id=`image-${it.id}`;
        img.src = it.download_url;
        item.appendChild(img);
        item.style.width=(it.width * scale)+'px';
        outline.appendChild(item);

        loadItem(isInPreviewMode, item, img, it);

      });
    });
};

document.onload = loadData();

view.addEventListener('click', event => {
  console.log('Clicked preview');
});

document.querySelector('.gallery .preview .close').addEventListener('click', event => {
  isInPreviewMode=false;
  view.style.display='none';
  outline.style.overflow='auto';
  outline.style.flexWrap='wrap';
  document.querySelectorAll('.gallery .outline .item').forEach(item => {
    let image = item.firstElementChild;
    image.width*=2;
    image.height*=2;
    item.style.width=image.width+'px';
    item.style.height=image.height+'px';
    setViewStyle(item, 'overview');
  });
});
